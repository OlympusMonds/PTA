import sys
import os

import geojson
import pony.orm as pny
from database import Location, init, Trip_Set


def read_geojson():
    with open("points.geojson", 'r') as f:
        gjson = geojson.load(f)

    coordinates = []
    for g in gjson["features"]:
        coordinates.append(g["geometry"]["coordinates"])

    return coordinates

def importer():
    coords = read_geojson()

    try:
        with pny.db_session:
            print("Total # locations: {}".format(len(coords)))
            for c in coords:
                lat_lon = "{}_{}".format(c[1], c[0])

                try:
                    l = Location[lat_lon]
                    #print("location: {} already exists".format(lat_lon))
                except pny.ObjectNotFound:
                    l = Location(lat_lon = lat_lon)
                    print("Added location: {}".format(lat_lon))

    except png.core.RollbackException:
        print("Error")


def generate_trip_sets():
    try:
        with pny.db_session:
            locations = pny.select(l for l in Location)[:]
            size = len(locations)**2
            print("Total # Trip_Sets: {}".format(size))
            count = 0
            for start_loc in locations:
                for end_loc in locations:
                    count += 1
                    if start_loc.lat_lon == end_loc.lat_lon:
                        continue  # Can't go to where you are!


                    # Does a trip set for that already exist?
                    ts_key = "{},{}".format(start_loc.lat_lon, end_loc.lat_lon)
                    try:
                        ts = Trip_Set[ts_key]
                    except pny.ObjectNotFound:
                        ts = Trip_Set(start_end = ts_key,
                                     start_loc = start_loc,
                                     end_loc = end_loc)
                        if count % 10000 == 0:
                            percentage = count / size * 100.
                            print("{:3.2f}% - Created new Trip_Set for {}".format(percentage, ts_key))



    except pny.core.RollbackException:
        logger.error("DB access failed. Retrying...")

   

if __name__ == "__main__":

    init()
    importer()
    #generate_trip_sets()

