from contextlib import contextmanager
import logging
from datetime import date
from pony.orm import Database, Required, Optional, Set, PrimaryKey, sql_debug, db_session
from pony.orm.core import RollbackException
from public_transport_analyser.database.db_details import dbhost, dbusername, dbpassword, dbname

# What is up with the scoping of this..
#db = Database("sqlite", "database.sqlite", create_db=False)
#db = Database('postgres', user=dbusername, password=dbpassword, host='localhost', database=dbname)
db = Database('mysql', host=dbhost, user=dbusername, password=dbpassword, database=dbname)

##############
# Util functions

def init():
    sql_debug(False)
    db.generate_mapping(check_tables=True, create_tables=True)


def create():
    db.generate_mapping(create_tables=True)


@contextmanager
def safe_db(logger):
    retries = 3
    for _ in range(retries):
        try:
            with db_session:
                yield
            break  # DB went OK
        except RollbackException:
            logger.error("DB dropped connection, retrying...")
    else:
        logger.error("DB failed big time")
        # TODO: deal with this error.



##############
# DB classes

class Location(db.Entity):
    """
    A location is just a point, where trip_sets can start or end
    """
    lat_lon = PrimaryKey(str)
    starting_trips = Set('Trip_Set', reverse='start_loc')
    ending_trips = Set('Trip_Set', reverse='end_loc')


class Trip(db.Entity):
    """
    A trip is a single journey, with it's start and end determined by
    its trip_set.
    """
    id = PrimaryKey(int, auto=True)
    public_trans = Required(bool)
    duration = Optional(int, size=24)
    trip_set = Required('Trip_Set')
    tod = Required(int, size=8)
    start_time = Optional(date)
    distance = Optional(float)
    done = Required(bool)


class Trip_Set(db.Entity):
    """
    A trip set is a set of trips, which all go from the same start and end locations
    A trip set would typically consist of a few public transport journeys and some 
    driving ones.
    """
    start_end = PrimaryKey(str)
    trips = Set(Trip)
    start_loc = Required(Location, reverse='starting_trips')
    end_loc = Required(Location, reverse='ending_trips')
