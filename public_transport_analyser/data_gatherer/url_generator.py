import arrow
import logging
import pony.orm as pny
from public_transport_analyser.data_gatherer import api
from public_transport_analyser.database.database import Trip, safe_db


def get_url(trip_id):

    logger = logging.getLogger('PTA.get_url     ')
    with safe_db(logger):
        logger.debug("Trying to access the DB for a start and end location")
 
        try:
            ts = Trip[trip_id]
            tod = ts.tod # make sure to include minutes
            start_coord = ts.trip_set.start_loc.lat_lon
            end_coord = ts.trip_set.end_loc.lat_lon
        except pny.ObjectNotFound:
            return None

    date = arrow.now('+10:00')
    if date.weekday() == 4:  # Friday
        date = date.shift(days=3)
    if date.weekday() == 5:  # Saturday
        date = date.shift(days=2)
    else:
        date = date.shift(days=1)

    date = date.format('YYYYMMDD')
    tod = "{:02d}00".format(tod)

    start_coord = "{1}:{0}".format(*start_coord.split('_'))
    end_coord   = "{1}:{0}".format(*end_coord.split('_'))

    url = ("https://api.transport.nsw.gov.au/v1/tp/trip?"
           "outputFormat=rapidJSON"
           "&coordOutputFormat=EPSG%3A4326"
           "&depArrMacro=dep"
           "&itdDate={date}"
           "&itdTime={tod}"
           "&type_origin=coord"
           "&name_origin={start_coord}:EPSG:4326"
           "&type_destination=coord"
           "&name_destination={end_coord}:EPSG:4326"
           "&calcNumberOfTrips=1"
           "&TfNSWTR=true"
           "&version=10.2.1.15").format(date = date,
                                        tod = tod,
                                        start_coord = start_coord,
                                        end_coord = end_coord)

    return url


