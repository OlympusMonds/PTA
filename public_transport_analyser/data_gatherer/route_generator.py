import random
import time
import logging
import pony.orm as pny
from public_transport_analyser.database.database import Location, Trip_Set, Trip, safe_db


def generate_routes_from_db(trip_event):
    logger = logging.getLogger('PTA.gen_routes  ')
    while True:
        logger.debug("Waiting for the OK to go ahead")
        trip_event.wait()  # Wait for the url_requestor to tell us to load up 
        logger.debug("Generating 10 new trip_sets")
        for _ in range(10):
            skip = False 
            with safe_db(logger):
                logger.debug("Access the DB for a start and end location")
                start_loc, end_loc = Location.select_random(limit=2)
                ts_key = "{},{}".format(start_loc.lat_lon, end_loc.lat_lon)

                # Does a trip set for that route already exist?
                try:
                    ts = Trip_Set[ts_key]
                    logger.debug("Existing Trip_Set found for {}. Skipping.".format(ts_key))
                    skip = True 
                except pny.ObjectNotFound:
                    ts = Trip_Set(start_end = ts_key,
                                  start_loc = start_loc,
                                  end_loc = end_loc)
                    logger.debug("Created new Trip_Set for {}".format(ts_key))


                # Now that we have a Trip_Set to deal with, generate some Trips
                if not skip:
                    hours = [6, 8, 12, 17, 21]
                    for hour in hours:
                        trip = Trip(public_trans = True,
                                    tod = hour,
                                    trip_set = ts,
                                    done = False)
                        trip.flush()  # to get the PK
                        logger.debug("Trip {ts} ({id}) (public transport) for time: {tod} inserted into queue.".format(ts=ts.start_end, id=trip.get_pk(), tod=hour))
                    """
                    # TODO figure out API for driving
                    hour = 12  # some random time..?
                    trip = Trip(public_trans = False,
                                tod = hour,
                                trip_set = ts)
                    trip.flush()  # to get the PK
                    trip_queue.put(trip.get_pk())
                    logger.debug("Trip {ts} ({id}) (driving) for time: {tod} inserted into queue.".format(ts=ts.start_end, id=trip.get_pk(), tod=hour))
                    """
                    time.sleep(1) # take it easy




