import sys
import queue
from threading import Thread, Event
import logging
import time

from public_transport_analyser.database.database import init
from public_transport_analyser.data_gatherer.route_generator import generate_routes_from_db
from public_transport_analyser.data_gatherer.url_requester import request_urls
from public_transport_analyser.data_gatherer.data_processor import process_data
from public_transport_analyser.data_gatherer.config import \
    max_daily_requests, queue_size, requester_threads


def setup_logging(log_to_file = True):
    logger = logging.getLogger('PTA')
    logger.setLevel(logging.DEBUG)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(threadName)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    if log_to_file:
        fh = logging.FileHandler('pta.log')
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    return logger


def main():
    logger = setup_logging(log_to_file=True)

    #trip_queue = queue.Queue(maxsize=queue_size)
    trip_event = Event()
    data_queue = queue.Queue(maxsize=queue_size)

    db = init()

    #bad_routes = set()

    trip_event.clear()

    # TODO: now we're using the DB, do we need a trip_queue? Can't we ask the DB: what trips aren't done?
    logger.info("Create route generating threads")
    route_thread = Thread(target=generate_routes_from_db, args=(trip_event,))
    route_thread.start()
    logger.info("Created route generating threads")

    logger.info("Create url requesting threads")
    for _ in range(requester_threads):
        url_request_thread = Thread(target=request_urls, args=(max_daily_requests/float(requester_threads), trip_event, data_queue))
        url_request_thread.start()
    logger.info("Created url requesting threads")

    logger.info("Create data processing thread")
    data_processing_thread = Thread(target=process_data, args=(data_queue,))
    data_processing_thread.start()
    logger.info("Created data processing thread")

    return 0


if __name__ == "__main__":
    sys.exit(main())






