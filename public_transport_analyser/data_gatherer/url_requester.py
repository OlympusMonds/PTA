import time
import datetime
import requests
import logging
import pony.orm as pny

from public_transport_analyser.database.database import Trip, safe_db
from public_transport_analyser.data_gatherer import api
from public_transport_analyser.data_gatherer.url_generator import get_url

def request_urls(max_daily_requests, trip_event, data_queue):
    """
    Request the trip from resource at a steady pace that does not exceed the usage
    limits. If the data is OK, save it to the database.
    :param max_daily_requests: int of how many requests we're allowed to do per day.
    :param trip_queue: The queue of trips to process
    :return: None, the while loop should run forever.
    """
    logger = logging.getLogger('PTA.request_urls')

    day_in_sec = 3600.*24.
    request_rate = day_in_sec / max_daily_requests

    total_requests_today = 0
    start_time = datetime.datetime.now()
    logger.debug("start_time = {}".format(start_time))

    while True:
        logger.debug("Telling route generator to wait")
        trip_event.clear()

        logger.debug("Request sleep period = {}".format(request_rate))
        logger.debug("Getting trip")
        #trip = trip_queue.get()
        
        with safe_db(logger):
            num_trips = pny.select(pny.count(t) for t in Trip if not t.done).first()
            if num_trips < 1000:
                trip_event.set()

            trip_obj = pny.select(t for t in Trip if not t.done).random(limit=1)[0]
            trip = trip_obj.get_pk()

        logger.debug("trip: {} - got.".format(trip))

        need_to_sleep = True

        url = get_url(trip)
        logger.debug("trip: {} - url to use: {}".format(trip, url))

        try:
            logger.debug("trip: {} - Requesting data".format(trip))
            r = requests.get(url, headers = {'Authorization': 'apikey {}'.format(api.apikey)})
            logger.debug("trip: {} - Got data".format(trip))

            if r.status_code == 200:
                reqjson = r.json()
                data_queue.put((trip, reqjson))

            else:
                logger.error("trip: {} - Bad response (response = {0}); skipped.".format(trip, r.status_code))

        except ConnectionError as ce:
            logger.error("trip: {} - Connection error. Computer says:\n{}".format(trip, ce))
            need_to_sleep = False
        except Exception as e:
            logger.error("trip: {} - Unknown exception caught. Computer says: {}".format(trip, e))


        #trip_queue.task_done()
        #logger.debug("trip: {} - released from queue.".format(trip))

        if need_to_sleep:
            logger.debug("Sleeping for {} seconds".format(request_rate))
            total_requests_today += 1
            time.sleep(request_rate)

        time_passed = (datetime.datetime.now() - start_time).total_seconds()
        if not total_requests_today == max_daily_requests:
            request_rate = (day_in_sec - time_passed) / (max_daily_requests - total_requests_today)
        logger.debug("Time passed today: {} seconds, request_rate: {} seconds".format(time_passed, request_rate))

        if time_passed >= day_in_sec:
            logger.info("24 hours passed. Resetting count to new day.")
            total_requests_today = 0
            request_rate = day_in_sec / max_daily_requests
            start_time = datetime.datetime.now()

        if total_requests_today >= max_daily_requests:
            seconds_until_tomorrow = day_in_sec - time_passed
            logger.error("Exceeded daily request limit. Sleeping until tomorrow (for {} seconds).".format(seconds_until_tomorrow))
            total_requests_today = 0
            time.sleep(seconds_until_tomorrow)

        logger.debug("Finished with trip {}".format(trip))
