import pony.orm as pny
import logging

from public_transport_analyser.database.database import Trip, safe_db
from public_transport_analyser.data_gatherer.PTEexceptions import ZeroResultsError



def process_data(data_queue):
    logger = logging.getLogger('PTA.data_process')

    while True:
        logger.debug("Getting trip and data")
        trip, reqjson = data_queue.get()
        logger.info("Got trip: {} and data".format(trip))

        try:
            logger.debug("trip: {} - Process data".format(trip))
            duration = process_response(reqjson)
            logger.info("trip: {} - Save to DB".format(trip))
            save_to_db(trip, duration)

        except ZeroResultsError:
            """
            ZeroResultsError typically means the route generator put the origin or the destination
            in a body of water or something.
            """
            logger.info("trip: {} - No results for that trip; deleting...")
            with safe_db(logger):
                Trip[trip].delete()
            """
            bad_routes.add(route_info["route"])
            if len(bad_routes) > 100e3:
                bad_routes.clear()  # Make sure things don't get out of hand.
            """

        except ValueError as e:
            logger.error("Returned data doesn't match expected data structure; result skipped - {}".format(e))

        finally:
            data_queue.task_done()


def process_response(reqjson):
    """
    Extract/process the data returned in the request to return the needed information.
    :param data: the JSON data of the request
    :return: list(duration, distance) of the particular trip.
    """
    logger = logging.getLogger('PTA.data_process')
    try:
        if 'journeys' in reqjson:
            journeys = reqjson['journeys']
            if len(journeys) == 0:
                raise ZeroResultsError("No results found")

            fj = journeys[0]

            duration = 0

            for l in fj['legs']:
                if 'duration' in l.keys():
                    duration += l['duration']
                else:
                    logger.debug("No duration found, looking for max cumDuration")
                    if 'footPathInfo' in l.keys() and 'pathDescriptions' in l.keys():
                        duration += max([pd['cumDuration'] for pd in l['pathDescriptions']])
                    else:
                        logger.error("No footpathINfo and pathDescriptions found")
                        raise ValueError(l)

        else:
            raise ZeroResultsError

    except (KeyError, IndexError) as e:
        print(l)
        raise ValueError("Exception: {0}".format(e))

    return duration


def save_to_db(trip, duration):
    """
    Save the route info to the DB. Search for existing DB entries first, and
    add to them if needed, otherwise generate new entries.
    :param route: string of the origin and destination, joined with a "_"
    :param duration: time in seconds of the trip
    :param distance: distance in meters of the trip
    :return: none
    """
    logger = logging.getLogger('PTA.database    ')

    with safe_db(logger):
        """
        Fetch the origin and dest from the DB, or create if they don't
        exist. Once obtained, make a new trip between them storing the
        results.
        """
        try:
            t = Trip[trip]
        except pny.ObjectNotFound:
            logger.error("trip: {} - trip not found in DB! This is not good!".format(trip))
            return -1

        t.duration = duration
        t.done = True

    logger.info("trip: {} - saved".format(trip))
